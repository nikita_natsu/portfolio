"use client"

import React, { useContext } from "react";
import { ToDoContext } from "../projects/todo-app/TodoContext";

const AddTodo = () => {
  const [inputValue, setInputValue] = React.useState("")
  const { setTodos } = useContext(ToDoContext)

  const onAdd = () => {
    const text = inputValue;
    setTodos(prevState => ([
      {
        id: Math.random() + Date.now() ,
        text,
        isChecked: false
      },
      ...prevState
    ]))
    setInputValue("")
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    onAdd()
  }

  return (
    <>
      <div class="h-100 w-full flex items-center justify-center bg-teal-lightest font-sans">
        <div class="bg-white rounded shadow p-6 m-4 w-full lg:w-3/4 lg:max-w-lg">
          <form onSubmit={handleSubmit}>
            
          <div class="mb-4">
            <h1 class="text-grey-darkest">Todo List</h1>
            <div class="flex mt-4">
              <input
                class="shadow appearance-none border rounded w-full py-2 px-3 mr-4 text-grey-darker"
                placeholder="Add Todo"
                value={inputValue}
                onChange={event => setInputValue(event.target.value)}
              />
              <button
                class="flex-no-shrink p-2 border-2 rounded text-teal border-teal hover:text-grey-darker hover:bg-teal"
                type="submit"
              >Add</button>
            </div>
          </div>
          </form>
        </div>
      </div>
    </>
  );
}

export default AddTodo;
