"use client"

import React, { useContext } from "react";
import { ToDoContext } from "../projects/todo-app/TodoContext";

const TodoList = ({ }) => {
  const { todos, setTodos } = useContext(ToDoContext)

  const handleNotDone = (id) => {
    const newTodos = [...todos]
    newTodos.map((t) => {
      if (t.id === id) {
        t.isChecked = false
      }
    })
    setTodos(newTodos)
  }
  const handleDone = (id) => {
    const newTodos = [...todos]
    newTodos.map((t) => {
      if (t.id === id) {
        t.isChecked = true
      }
    })
    setTodos(newTodos)
  }

  const removeTodo = (id) => {
    const newTodos = [...todos].filter((t) => {
      if (t.id === id) {
        return false
      }

      return true
    })
    setTodos(newTodos)
  }

  return (
    <>
      <div>
        {todos.map(({ id, text, isChecked }) => (
          <div class="bg-white rounded shadow p-6 m-4 w-full lg:w-3/4 lg:max-w-lg mb-4"
            style={{
              marginLeft: "auto",
              marginRight: "auto",
              background: isChecked ? "#a5ddae" : "#f1f4f5"
            }}
            key={id}
          >
            <div class="flex mb-4 items-center">
              <p class="w-full text-grey-darkest">
                {!isChecked && (
                  <button
                    class="flex-no-shrink p-2 border-2 rounded text-green border-green hover:bg-green"
                    // style={{color: "#aa021a"}}
                    onClick={() => handleDone(id)}
                  >Done</button>
                )}

                {isChecked && (
                  <button
                    class="flex-no-shrink p-2 border-2 rounded hover:text-red text-grey border-grey hover:bg-grey"
                    onClick={() => handleNotDone(id)}
                  >Not Done</button>
                )}

              </p>
              <button
                onClick={() => removeTodo(id)}
                style={{ color: "#aa021a" }}
                class="flex-no-shrink p-2 ml-2 border-2 rounded text-red border-red hover:text-white hover:bg-red"
              >
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                  <path strokeLinecap="round" strokeLinejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" />
                </svg>
              </button>
            </div>
            <div class="flex mb-4 items-center">
              {text}
            </div>
          </div>
        ))}
      </div>
    </>
  );
}
export default TodoList;
