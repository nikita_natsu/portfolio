"use client"

import React, { useState } from "react";
import Link from "next/link";
// import { allProjects } from "contentlayer/generated";
import { Navigation } from "../components/nav";
import { Card } from "../components/card";
import { Article } from "./article";
// import { Redis } from "@upstash/redis";
// import { Eye } from "lucide-react";

// const redis = Redis.fromEnv();

export const revalidate = 60;
export default async function ProjectsPage() {

  const featured = {
    title: "To-Do App",
    description: "A powerful and intuitive task management application designed and developed by me. This feature-rich Todo app provides users with a seamless and organized way to manage their daily tasks and stay productive.",
    slug: "todo-app",
    buttonText: "Demo App",
  }

  const projects = [
    // {
    //   // slug: "this-code",
    //   title: "View the To-Do app code.",
    //   description: "Explore the codebase of this portfolio, including the implementation of the To-Do app.",
    //   backgroundColor: "#092d3e",
    //   // titleColor: "",
    //   descriptionColor: "aliceblue",
    //   buttonText: "Explore Code",
    // },
    {
      slug: "/why-the-code",
      title: "Code Explanation.",
      description: "This project is built with 4 files working together. It includes a context file that enables state sharing between two component files. The add todo component and the list todo component are the two files that utilize the same state. These files are invoked from the page.jsx file, which includes the provider from the context. This allows the two components to successfully share state.",
      backgroundColor: "#092d3e",
      // titleColor: "",
      descriptionColor: "aliceblue",
      buttonText: "Read Article",
    }
  ]


  // const views = (
  // 	await redis.mget<number[]>(
  // 		...allProjects.map((p) => ["pageviews", "projects", p.slug].join(":")),
  // 	)
  // ).reduce((acc, v, i) => {
  // 	acc[allProjects[i].slug] = v ?? 0;
  // 	return acc;
  // }, {} as Record<string, number>);

  // const featured = allProjects.find(
  // 	(project) => project.slug === "planetfall",
  // )!;
  // const top2 = allProjects.find((project) => project.slug === "highstorm")!;
  // const top3 = allProjects.find((project) => project.slug === "envshare")!;
  // const sorted = allProjects
  // 	.filter((p) => p.published)
  // 	.filter(
  // 		(project) =>
  // 			project.slug !== featured.slug &&
  // 			project.slug !== top2.slug &&
  // 			project.slug !== top3.slug,
  // 	)
  // 	.sort(
  // 		(a, b) =>
  // 			new Date(b.date ?? Number.POSITIVE_INFINITY).getTime() -
  // 			new Date(a.date ?? Number.POSITIVE_INFINITY).getTime(),
  // 	);

  return (
    <div className="relative pb-16">
      <Navigation />
      <div className="px-6 pt-16 mx-auto space-y-8 max-w-7xl lg:px-8 md:space-y-16 md:pt-24 lg:pt-32">
        <div className="max-w-2xl mx-auto lg:mx-0">
          <h2 className="text-3xl font-bold tracking-tight text-zinc-100 sm:text-4xl">
            Projects
          </h2>
          <p className="mt-4 text-zinc-400">
            Some of the projects are from work and some are on my own time.
          </p>
        </div>
        <div className="w-full h-px bg-zinc-800" />

        <div className="grid grid-cols-1 gap-8 mx-auto lg:grid-cols-2 ">
          <Card>
            <Link href={`/projects/${featured.slug}`}>
              <article className="relative w-full h-full p-4 md:p-8">

                <h2
                  id="featured-post"
                  className="mt-4 text-3xl font-bold text-zinc-100 group-hover:text-white sm:text-4xl font-display"
                >
                  {featured.title}
                </h2>
                <p className="mt-4 mb-4 leading-8 duration-150 text-zinc-400 group-hover:text-zinc-300">
                  {featured.description}
                </p>
                <div className="bottom-4 md:bottom-8">
                  <button style={{ backgroundColor: "#0b4a68", color: "aliceblue" }} className="bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow">
                    {featured.buttonText}
                  </button>
                  {/* <p className="hidden text-zinc-200 hover:text-zinc-50 lg:block">
                    Read more <span aria-hidden="true">&rarr;</span>
                  </p> */}
                </div>
              </article>
            </Link>
          </Card>

          <div className="flex flex-col w-full gap-8 mx-auto border-t border-gray-900/10 lg:mx-0 lg:border-t-0 ">
            {projects.map((project) => (
              <Card key={project.slug}>
                <Article project={project} />
              </Card>
            ))}
          </div>
        </div>
        <div className="hidden w-full h-px md:block bg-zinc-800" />

        {/* <div className="grid grid-cols-1 gap-4 mx-auto lg:mx-0 md:grid-cols-3">
					<div className="grid grid-cols-1 gap-4">
						{sorted
							.filter((_, i) => i % 3 === 0)
							.map((project) => (
								<Card key={project.slug}>
									<Article project={project} views={views[project.slug] ?? 0} />
								</Card>
							))}
					</div>
					<div className="grid grid-cols-1 gap-4">
						{sorted
							.filter((_, i) => i % 3 === 1)
							.map((project) => (
								<Card key={project.slug}>
									<Article project={project} views={views[project.slug] ?? 0} />
								</Card>
							))}
					</div>
					<div className="grid grid-cols-1 gap-4">
						{sorted
							.filter((_, i) => i % 3 === 2)
							.map((project) => (
								<Card key={project.slug}>
									<Article project={project} views={views[project.slug] ?? 0} />
								</Card>
							))}
					</div> */}
        {/* </div> */}
      </div>
    </div>
  );
}
