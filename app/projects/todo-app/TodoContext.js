"use client"

import { useState, createContext, useContext } from "react";

export const ToDoContext = createContext();

function TodoProvider({ children }) {
  const [todos, setTodos] = useState([
    {
      id: 1,
      text: "Study",
      isChecked: false
    }
  ]);

  return (
    <ToDoContext.Provider value={{todos, setTodos}}>
      {children}
    </ToDoContext.Provider>
  );
}

export default TodoProvider