"use client"

import React from "react"
import { Header } from "./header";
import AddTodo from "../../components/AddTodo"
import TodoList from "../../components/TodoList"
import TodoProvider from "./TodoContext"

export default async function PostPage() {

  return (
    <TodoProvider>
      <div className="bg-zinc-50 min-h-screen" style={{backgroundColor: "#dae0e5"}}>
        <Header />
        <div style={{ maxWidth: "740px", margin: "auto", minHeight: "100vh" }}>
          <AddTodo />
          <TodoList /> 
        </div>
      </div>
    </TodoProvider>
  );
}
