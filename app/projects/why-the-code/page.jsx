"use client"

import React from "react"
import { Header } from "./header";
import SyntaxHighlighter from 'react-syntax-highlighter';
import { docco } from 'react-syntax-highlighter/dist/esm/styles/hljs';

export default async function PostPage() {
  const project = {
    title: "How I built this to do app.",
    description: "Unleash Your Potential and Demonstrate Your Organizational Skills",
  }

  return (
    <div className="bg-zinc-50 min-h-screen" style={{ backgroundColor: "#dae0e5" }}>
      <Header project={project} />

      <div style={{ maxWidth: "740px", margin: "auto", minHeight: "100vh" }}>
        <br />

        <p>
        This project is built using four files in conjunction. A context file is created to share state between two component files: the 'add todo' component and the 'list todo' component. These two files are called from the 'page.jsx' file, which also includes the context provider. This setup enables successful state sharing between the two components.
        </p>
        <br />

        <p>ToDoContext.jsx</p>
        <SyntaxHighlighter language="javascript" style={docco}>
          {`
"use client"

import { useState, createContext, useContext } from "react";

export const ToDoContext = createContext();

function TodoProvider({ children }) {
  const [todos, setTodos] = useState([
    {
      id: 1,
      text: "Study",
      isChecked: false
    }
  ]);

  return (
    <ToDoContext.Provider value={{todos, setTodos}}>
      {children}
    </ToDoContext.Provider>
  );
}

export default TodoProvider
`}
        </SyntaxHighlighter>
        <br />

        <p>AddToDo.jsx</p>
        <SyntaxHighlighter language="javascript" style={docco}>
          {`
"use client"

import React, { useContext } from "react";
import { ToDoContext } from "../projects/todo-app/TodoContext";

const AddTodo = () => {
  const [inputValue, setInputValue] = React.useState("")
  const { setTodos } = useContext(ToDoContext)

  const onAdd = () => {
    const text = inputValue;
    setTodos(prevState => ([
      {
        id: Math.random() + Date.now() ,
        text,
        isChecked: false
      },
      ...prevState
    ]))
    setInputValue("")
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    onAdd()
  }

  return (
    <>
      <div class="h-100 w-full flex items-center justify-center bg-teal-lightest font-sans">
        <div class="bg-white rounded shadow p-6 m-4 w-full lg:w-3/4 lg:max-w-lg">
          <form onSubmit={handleSubmit}>
            
          <div class="mb-4">
            <h1 class="text-grey-darkest">Todo List</h1>
            <div class="flex mt-4">
              <input
                class="shadow appearance-none border rounded w-full py-2 px-3 mr-4 text-grey-darker"
                placeholder="Add Todo"
                value={inputValue}
                onChange={event => setInputValue(event.target.value)}
              />
              <button
                class="flex-no-shrink p-2 border-2 rounded text-teal border-teal hover:text-grey-darker hover:bg-teal"
                type="submit"
              >Add</button>
            </div>
          </div>
          </form>
        </div>
      </div>
    </>
  );
}

export default AddTodo;

`}
        </SyntaxHighlighter>
        <br />
        <p>ToDoList.jsx</p>
        <SyntaxHighlighter language="javascript" style={docco}>
          {`
"use client"

import React, { useContext } from "react";
import { ToDoContext } from "../projects/todo-app/TodoContext";

const TodoList = ({ }) => {
  const { todos, setTodos } = useContext(ToDoContext)

  const handleNotDone = (id) => {
    const newTodos = [...todos]
    newTodos.map((t) => {
      if (t.id === id) {
        t.isChecked = false
      }
    })
    setTodos(newTodos)
  }
  const handleDone = (id) => {
    const newTodos = [...todos]
    newTodos.map((t) => {
      if (t.id === id) {
        t.isChecked = true
      }
    })
    setTodos(newTodos)
  }

  const removeTodo = (id) => {
    const newTodos = [...todos].filter((t) => {
      if (t.id === id) {
        return false
      }

      return true
    })
    setTodos(newTodos)
  }

  return (
    <>
      <div>
        {todos.map(({ id, text, isChecked }) => (
          <div class="bg-white rounded shadow p-6 m-4 w-full lg:w-3/4 lg:max-w-lg mb-4"
            style={{
              marginLeft: "auto",
              marginRight: "auto",
              background: isChecked ? "#a5ddae" : "#f1f4f5"
            }}
            key={id}
          >
            <div class="flex mb-4 items-center">
              <p class="w-full text-grey-darkest">
                {!isChecked && (
                  <button
                    class="flex-no-shrink p-2 border-2 rounded text-green border-green hover:bg-green"
                    // style={{color: "#aa021a"}}
                    onClick={() => handleDone(id)}
                  >Done</button>
                )}

                {isChecked && (
                  <button
                    class="flex-no-shrink p-2 border-2 rounded hover:text-red text-grey border-grey hover:bg-grey"
                    onClick={() => handleNotDone(id)}
                  >Not Done</button>
                )}

              </p>
              <button
                onClick={() => removeTodo(id)}
                style={{ color: "#aa021a" }}
                class="flex-no-shrink p-2 ml-2 border-2 rounded text-red border-red hover:text-white hover:bg-red"
              >
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                  <path strokeLinecap="round" strokeLinejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" />
                </svg>
              </button>
            </div>
            <div class="flex mb-4 items-center">
              {text}
            </div>
          </div>
        ))}
      </div>
    </>
  );
}
export default TodoList;

      `}
        </SyntaxHighlighter>
        <br />
        <p>page.jsx</p>
        <SyntaxHighlighter language="javascript" style={docco}>
          {`

"use client"

import React from "react"
import { Header } from "./header";
import AddTodo from "../../components/AddTodo"
import TodoList from "../../components/TodoList"
import TodoProvider from "./TodoContext"

export default async function PostPage() {

  return (
    <TodoProvider>
      <div className="bg-zinc-50 min-h-screen" style={{backgroundColor: "#dae0e5"}}>
        <Header />
        <div style={{ maxWidth: "740px", margin: "auto", minHeight: "100vh" }}>
          <AddTodo />
          <TodoList /> 
        </div>
      </div>
    </TodoProvider>
  );
}

`}
        </SyntaxHighlighter>
        <br />



      </div>
      {/* <article className="px-4 py-12 mx-auto prose prose-zinc prose-quoteless">
				<Mdx code={project.body?.code} />
			</article> */}
    </div>
  );
}
