import Link from "next/link";
import React from "react";
import Particles from "./components/particles";
// import Particles from "../components/particles";

const navigation = [
  { name: "Projects", href: "/projects", className: "bg-gray-100 hover:bg-gray-500 text-gray-800 font-bold py-2 px-4 rounded-l" },
  { name: "Contact", href: "/contact", className: "bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-r" },
];

export default function Home() {
  return (
    <div className="flex flex-col items-center justify-center w-screen h-screen overflow-hidden bg-gradient-to-tl from-black via-zinc-600/20 to-black">
      {/* <nav className="my-16 animate-fade-in">
        <ul className="flex items-center justify-center gap-4">
          {navigation.map((item) => (
            <Link
              key={item.href}
              href={item.href}
              className="text-sm duration-500 text-zinc-500 hover:text-zinc-300"
            >
              {item.name}
            </Link>
          ))}
        </ul>
      </nav> */}
      <div className="hidden w-screen h-px animate-glow md:block animate-fade-left bg-gradient-to-r from-zinc-300/0 via-zinc-300/50 to-zinc-300/0" />
      <Particles
        className="absolute inset-0 -z-10 animate-fade-in"
        quantity={100}
      />
      <h1 className="z-10 text-4xl text-transparent duration-1000 bg-white cursor-default text-edge-outline animate-title font-display sm:text-6xl md:text-9xl whitespace-nowrap bg-clip-text ">
        Michael's Portfolio
      </h1>

      <div className="hidden w-screen h-px animate-glow md:block animate-fade-right bg-gradient-to-r from-zinc-300/0 via-zinc-300/50 to-zinc-300/0" />
      <div className="my-16 text-center animate-fade-in">

        <h2 className="text-sm text-zinc-500 mb-5">
          As a software developer, I thrive on the creative process of turning ideas into functional and efficient solutions through code.
        </h2>
        <div className="inline-flex">

          {navigation.map((item) => {
            return (
              <Link
                key={item.href}
                href={item.href}
                className="text-sm duration-500 text-zinc-500 hover:text-zinc-300"
              >
                <button className={item.className}>
                  {item.name}
                </button>
              </Link>
            )
          })}
          {/* <button className="bg-gray-100 hover:bg-gray-500 text-gray-800 font-bold py-2 px-4 rounded-l">
            Projects
          </button>
          <button className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-r">
            Contact
          </button> */}
        </div>
      </div>
    </div>
  );
}
